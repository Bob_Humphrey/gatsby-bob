const path = require(`path`);

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions;

  const result = await graphql(
    `
      {
        allDirectusRecentWork {
          edges {
            node {
              directusId
            }
          }
        }
        allDirectusRandall {
          edges {
            node {
              directusId
            }
          }
        }
        allMarkdownRemark(
          sort: { order: DESC, fields: [frontmatter___date] }
          filter: { frontmatter: { published: { eq: true } } }
          limit: 200
        ) {
          edges {
            node {
              frontmatter {
                path
              }
            }
          }
        }
      }
    `
  );

  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`);
    return;
  }

  const paginationPath = (path, page, totalPages) => {
    if (page === 0) {
      return path;
    } else if (page < 0 || page >= totalPages) {
      return ``;
    } else {
      return `${path}${page + 1}`;
    }
  };

  const items = result.data.allDirectusRecentWork.edges;
  const randallItems = result.data.allDirectusRandall.edges;
  // How many items do we have?
  const itemsCount = items.length;
  const randallItemsCount = randallItems.length;
  // How many items per paginated page?
  const itemsPerPaginatedPage = 10;
  const randallItemsPerPaginatedPage = 10;
  // How many paginated pages do we need?
  const paginatedPagesCount = Math.ceil(itemsCount / itemsPerPaginatedPage);
  const randallPaginatedPagesCount = Math.ceil(
    randallItemsCount / randallItemsPerPaginatedPage
  );

  // Create items list pages

  let index = 0;
  for (index = 0; index < paginatedPagesCount; index++) {
    createPage({
      // Calculate the path for this page like `/blog`, `/blog/2`
      path: paginationPath(`/work`, index, paginatedPagesCount),
      // Set the component as normal
      component: path.resolve(`./src/templates/WorkList.js`),
      // Pass the following context to the component
      context: {
        // Skip this number of items from the beginning
        skip: index * itemsPerPaginatedPage,
        // How many items to show on this paginated page
        limit: itemsPerPaginatedPage,
        // How many paginated pages there are in total
        paginatedPagesCount,
        // The path to the previous paginated page (or an empty string)
        prevPath: paginationPath(`/work`, index - 1, paginatedPagesCount),
        // The path to the next paginated page (or an empty string)
        nextPath: paginationPath(`/work`, index + 1, paginatedPagesCount)
      }
    });
  }

  // Create Randall Library projects pages

  for (index = 0; index < randallPaginatedPagesCount; index++) {
    createPage({
      // Calculate the path for this page like `/blog`, `/blog/2`
      path: paginationPath(`/projects`, index, randallPaginatedPagesCount),
      // Set the component as normal
      component: path.resolve(`./src/templates/ProjectsList.js`),
      // Pass the following context to the component
      context: {
        // Skip this number of items from the beginning
        skip: index * randallItemsPerPaginatedPage,
        // How many items to show on this paginated page
        limit: randallItemsPerPaginatedPage,
        // How many paginated pages there are in total
        randallPaginatedPagesCount,
        // The path to the previous paginated page (or an empty string)
        prevPath: paginationPath(
          `/projects`,
          index - 1,
          randallPaginatedPagesCount
        ),
        // The path to the next paginated page (or an empty string)
        nextPath: paginationPath(
          `/projects`,
          index + 1,
          randallPaginatedPagesCount
        )
      }
    });
  }

  // Create articles pages

  const articleTemplate = path.resolve(`src/templates/ArticleTemplate.js`);

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    console.log("article being created");
    createPage({
      path: node.frontmatter.path,
      component: articleTemplate,
      context: {} // additional data can be passed via context
    });
  });
};
