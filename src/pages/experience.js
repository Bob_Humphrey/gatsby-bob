import React from "react";
import { StaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";
import { motion } from "framer-motion";
import Layout from "../components/layout";
import Roadway from "../components/Roadway";

const RANDALL_LIST = graphql`
  query RandallList {
    allFile(
      filter: {
        relativeDirectory: { eq: "randall" }
        childImageSharp: { fluid: {} }
      }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`;

const variants = {
  hidden: {
    opacity: 0
  },
  visible: { opacity: 1 },
  transition: { duration: 1 }
};

const Experience = () => {
  return (
    <Layout>
      <motion.div initial="hidden" animate="visible" variants={variants}>
        <div className="w-11/12 lg:w-1/2 mx-auto mb-10">
          <h2 className="text-3xl font-inter_bold text-blue-800 leading-none mb-10">
            Experience
          </h2>
          <h3 className="text-2xl font-inter_bold text-blue-500 mb-6">
            University of North Carolina Wilmington (2011-2017)
          </h3>

          <div className="font-inter_regular text-lg text-gray-700 text-justify leading-normal">
            <p className="mb-4">
              For 6 years I was the Web and Applications Developer for{" "}
              <a
                className="text-blue-500"
                href="https://library.uncw.edu/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Randall Library
              </a>{" "}
              at{" "}
              <a
                className="text-blue-500"
                href="https://uncw.edu/"
                target="_blank"
                rel="noopener noreferrer"
              >
                The University of North Carolina Wilmington
              </a>
              . This is a busy academic library that sees 6 to 7,000 students
              come through its doors on a daily basis, with several thousand
              more visiting its website.
            </p>
            <p>
              I was the library's only coder, and I was responsible for the full
              stack -- front end, back end, and servers. I completed{" "}
              <a className="text-blue-500" href="/projects">
                dozens of projects
              </a>{" "}
              that helped the university's students and faculty make better use
              of their library. In the process of doing this, I put together a
              pretty extensive{" "}
              <a className="text-blue-500" href="/skills">
                set of skills
              </a>
              .
            </p>
            <div className="grid grid-cols-2 lg:grid-cols-3 gap-4 mt-6 pb-24 mb-24 ">
              <StaticQuery
                query={RANDALL_LIST}
                render={({ allFile }) =>
                  allFile.edges.map(({ node }) => (
                    <Img
                      className=""
                      alt="Randall Library"
                      fluid={node.childImageSharp.fluid}
                    />
                  ))
                }
              />
            </div>
          </div>
          <Roadway />
        </div>
      </motion.div>
    </Layout>
  );
};

export default Experience;
