import React from "react";
import { motion } from "framer-motion";
import Layout from "../components/layout";

const IndexPage = () => {
  const variants = {
    hidden: {
      opacity: 0
    },
    visible: { opacity: 1 }
  };

  return (
    <Layout>
      <h2 className="w-11/12 lg:w-1/2 text-3xl font-inter_bold leading-none text-blue-800 leading-none mx-auto pb-10">
        Skills
      </h2>
      <motion.div initial="hidden" animate="visible" variants={variants}>
        <div className="lg:flex w-1/2 mx-auto font-inter_regular text-lg text-gray-700">
          <div className="lg:w-1/3">
            <h3 className="text-2xl font-inter_medium text-blue-500 leading-none pb-6">
              JavaScript
            </h3>
            <ul className="">
              <li className="">Gatsby</li>
              <li className="">React</li>
              <li className="">Node / npm</li>
              <li className="">Vue</li>
              <li className="">jQuery</li>
              <li className="">Express</li>
            </ul>
            <h3 className="text-2xl font-inter_medium text-blue-500 leading-none py-6">
              PHP
            </h3>
            <ul className="">
              <li className="">Laravel</li>
              <li className="">Yii</li>
              <li className="">Drupal coding</li>
              <li className="">Wordpress coding</li>
            </ul>
            <h3 className="text-2xl font-inter_medium text-blue-500 leading-none py-6">
              Database
            </h3>
            <ul className="">
              <li className="">SQL</li>
              <li className="">GraphQL</li>
              <li className="">MySQL</li>
              <li className="">phpMyAdmin</li>
              <li className="">PostgreSQL</li>
              <li className="">Microsoft Access</li>
            </ul>
          </div>

          <div className="lg:w-1/3">
            <h3 className="text-2xl font-inter_medium text-blue-500 leading-none pb-6">
              Front End
            </h3>
            <ul className="">
              <li className="">HTML</li>
              <li className="">CSS</li>
              <li className="">Tailwind CSS</li>
              <li className="">Bootstrap</li>
              <li className="">Zurb Foundation</li>
              <li className="">Bulma</li>
            </ul>
            <h3 className="text-2xl font-inter_medium text-blue-500 leading-none py-6">
              Static Sites
            </h3>
            <ul className="">
              <li className="">Gatsby</li>
              <li className="">Jigsaw</li>
              <li className="">Hugo</li>
              <li className="">Jekyll</li>
            </ul>{" "}
            <h3 className="text-2xl font-inter_medium text-blue-500 leading-none py-6">
              Content Mgt
            </h3>
            <ul className="">
              <li className="">Directus</li>
              <li className="">Cockpit</li>
              <li className="">Drupal</li>
              <li className="">WordPress</li>
              <li className="">Joomla</li>
            </ul>
            <h3 className="text-2xl font-inter_medium text-blue-500 leading-none py-6">
              Graphics
            </h3>
            <ul className="">
              <li className="">Photoshop</li>
              <li className="">Illustrator</li>
              <li className="">Canon Digital Photo Professional</li>
              <li className="">Riot - Radical Image Optimization Tool</li>
            </ul>
          </div>

          <div className="lg:w-1/3">
            <h3 className="text-2xl font-inter_medium text-blue-500 leading-none pb-6">
              DevOps
            </h3>
            <ul className="">
              <li className="">Windows</li>
              <li className="">Linux</li>
              <li className="">Ubuntu</li>
              <li className="">Red Hat</li>
              <li className="">CentOS</li>
              <li className="">Digital Ocean</li>
              <li className="">Linode</li>
              <li className="">PuTTY</li>
              <li className="">FileZilla</li>
            </ul>
            <h3 className="text-2xl font-inter_medium text-blue-500 leading-none py-6">
              Misc
            </h3>
            <ul className="Misc">
              <li className="">Gitlab</li>
              <li className="">Github</li>
              <li className="">Visual Studio</li>
              <li className="">Firefox Web Development Tools</li>
              <li className="">Markdown</li>
              <li className="">Postwoman</li>
              <li className="">Postman</li>
              <li className="">Lighthouse</li>
              <li className="">Bash</li>
            </ul>
          </div>
        </div>
      </motion.div>
    </Layout>
  );
};

export default IndexPage;
