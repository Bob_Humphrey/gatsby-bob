import React from "react";
import { graphql } from "gatsby";
import { motion } from "framer-motion";
import Layout from "../components/layout";
import ArticleLink from "../components/ArticleLink";

const variants = {
  hidden: {
    opacity: 0
  },
  visible: { opacity: 1 },
  transition: { duration: 1 }
};

const ArticlePage = ({
  data: {
    allMarkdownRemark: { edges }
  }
}) => {
  const Articles = edges.map(edge => (
    <ArticleLink key={edge.node.id} post={edge.node} />
  ));
  return (
    <Layout>
      <motion.div initial="hidden" animate="visible" variants={variants}>
        <div className="w-11/12 lg:w-1/2 mx-auto mb-10">
          <h2 className="text-3xl font-inter_bold text-blue-800 leading-none mb-10">
            Articles
          </h2>
          {Articles}
        </div>
      </motion.div>
    </Layout>
  );
};
export default ArticlePage;

export const pageQuery = graphql`
  query {
    allMarkdownRemark(
      sort: { order: DESC, fields: [frontmatter___date] }
      filter: { frontmatter: { published: { eq: true } } }
    ) {
      edges {
        node {
          id
          frontmatter {
            date(formatString: "MMMM D, YYYY")
            path
            title
          }
        }
      }
    }
  }
`;
