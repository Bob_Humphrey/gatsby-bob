import React from "react";
import { useStaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";
import { motion } from "framer-motion";
import Layout from "../components/layout";

const IndexPage = () => {
  const dogImg = useStaticQuery(graphql`
    query DogQuery {
      file(name: { eq: "dog1" }) {
        childImageSharp {
          # Specify the image processing specifications right in the query.
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);
  const variants = {
    hidden: {
      opacity: 0
    },
    visible: { opacity: 1 }
  };
  return (
    <Layout>
      <motion.div initial="hidden" animate="visible" variants={variants}>
        <div className="w-11/12 lg:flex lg:w-1/2 mx-auto text-black text-lg">
          <div className="lg:w-2/5">
            <div className="hidden lg:block mb-0 ">
              <Img
                className=""
                alt="Dog Smile Factory"
                fluid={dogImg.file.childImageSharp.fluid}
              />
            </div>
          </div>
          <div className="lg:w-3/5 lg:pl-12 text-lg font-inter_regular text-gray-700 leading-normal text-justify">
            <p className="mb-20">
              <span className="text-2xl font-inter_medium">
                Hi, my name is Bob.
              </span>{" "}
              I've been a software developer for many years. For me, programming
              leads to that wonderful state of flow, where you are completely
              and happily immersed in the thing you are doing. I enjoy building
              applications that have Laravel back ends and React front ends. I'm
              also pretty good with databases and content management systems.
            </p>
          </div>
        </div>
      </motion.div>
    </Layout>
  );
};

export default IndexPage;
