import React from "react";
import { Link } from "gatsby";

const PageNavigation = ({ props }) => {
  const prevDisplay = props.prevPath ? `flex` : `hidden`;
  const nextDisplay = props.nextPath ? `flex` : `hidden`;
  return (
    <div className="flex justify-between font-inter_light">
      <div className="flex justify-start">
        <div className={prevDisplay}>
          <Link
            to={props.prevPath}
            className="hover:bg-blue-500 text-blue-500 hover:text-white border border-blue-500 text-center rounded py-2 w-24 lg:w-48"
          >
            {" "}
            Previous
          </Link>
        </div>
      </div>
      <div className="flex justify-end">
        <div className={nextDisplay}>
          <Link
            to={props.nextPath}
            className="hover:bg-blue-500 text-blue-500 hover:text-white border border-blue-500 text-center rounded py-2 w-24 lg:w-48"
          >
            {" "}
            Next
          </Link>
        </div>
      </div>
    </div>
  );
};

export default PageNavigation;
