import React from "react";
import Img from "gatsby-image";

const ImageItem = ({ props }) => {
  return (
    <div>
      <Img
        alt=""
        className="rounded-lg shadow-lg mb-8"
        fluid={props.image.localFile.childImageSharp.fluid}
      />
    </div>
  );
};

export default ImageItem;
