import React from "react";
import ImageList from "./ImageList";

const RecentWork = ({ props, bgColor }) => {
  let tagStr = "";
  props.tags.forEach(function(item, index, array) {
    tagStr = tagStr + item;
    if (index < array.length - 1) {
      tagStr = tagStr + " / ";
    }
  });
  return (
    <article
      className={bgColor + " w-full font-inter_regular text-gray-700 py-24"}
      key={props.directusId}
    >
      <div className=" w-11/12 lg:w-1/2 mx-auto">
        <ImageList className="w-full" props={props} />
        <h3 className="w-full text-3xl font-inter_bold text-blue-500 text-center py-6">
          {props.name}
        </h3>
        <div className="w-full md:w-4/5 text-lg text-justify mx-auto pb-8">
          {props.description}
        </div>
        <div className="hidden md:flex justify-center">
          {props.tags.map(item => (
            <div className="text-sm text-center lg:border border-grey-600 rounded px-2 py-1 mr-2 mb-6">
              {item}
            </div>
          ))}
        </div>
        <div className="md:hidden justify-center text-sm text-center mb-6">
          {tagStr}
        </div>
        <div className="flex justify-center text-sm">
          <div className="text-white text-center bg-gray-600 hover:bg-black rounded px-2 py-1 mr-2">
            <a
              className=""
              href={props.url}
              target="_blank"
              rel="noopener noreferrer"
            >
              Visit Site
            </a>
          </div>
          <div className="text-white text-center bg-gray-600 hover:bg-black rounded px-2 py-1 mr-2">
            <a
              className=""
              href={props.code_url}
              target="_blank"
              rel="noopener noreferrer"
            >
              View Code
            </a>
          </div>
          {props.additional_code_url === null ? null : (
            <div className="text-white text-center bg-gray-600 hover:bg-black rounded px-2 py-1 mr-2">
              <a
                className=""
                href={props.additional_code_url}
                target="_blank"
                rel="noopener noreferrer"
              >
                Additional Code
              </a>
            </div>
          )}
          {props.learn_more_url === null ? null : (
            <div className="text-white text-center bg-gray-600 hover:bg-black rounded px-2 py-1 mr-2">
              <a className="" href={props.learn_more_url}>
                Learn More
              </a>
            </div>
          )}
        </div>
      </div>
    </article>
  );
};

export default RecentWork;
