import React from "react";
import { Link } from "gatsby";

const Header = () => {
  return (
    <header className="bg-gray-50 py-2">
      <div className="flex flex-col lg:flex-row w-full lg:justify-between lg:px-32">
        <div className="flex lg:justify-start pt-5 pb-6">
          <Link
            to={`/`}
            className="flex flex-col lg:flex-row w-full text-center lg:text-left text-blue-900 no-underline lg:leading-none"
          >
            <h1 className="self-center font-inter_bold text-3xl ">
              Bob Humphrey
            </h1>

            <div className="self-center font-inter_light text-3xl text-blue-500 lg:pl-4">
              Web Developer
            </div>
          </Link>
        </div>
        <div className="flex flex-col lg:flex-row lg:justify-end text-lg font-inter_light lg:text-xl leading-none pb-8 lg:pb-0">
          <Link
            to={`/experience`}
            className="text-blue-900 hover:text-blue-500 self-center
            no-underline px-2 py-2"
          >
            {" "}
            Experience
          </Link>
          <Link
            to={`/work`}
            className="text-blue-900 hover:text-blue-500 self-center
            no-underline px-2 py-2"
          >
            {" "}
            Recent Work
          </Link>
          <Link
            to={`/projects`}
            className="text-blue-900 hover:text-blue-500 self-center
            no-underline px-2 py-2"
          >
            {" "}
            Projects
          </Link>
          <Link
            to={`/skills`}
            className="text-blue-900 hover:text-blue-500 self-center
            no-underline px-2 py-2"
          >
            {" "}
            Skills
          </Link>
          <Link
            to={`/articles`}
            className="text-blue-900 hover:text-blue-500 self-center
            no-underline px-2 py-2"
          >
            {" "}
            Articles
          </Link>
        </div>
      </div>
    </header>
  );
};

export default Header;
