import React from "react";
import ImageItem from "./ImageItem";

const ImageList = ({ props }) => {
  if (props.screenshots) {
    return props.screenshots.map(item => (
      <ImageItem className="w-full" key={item.directusId} props={item} />
    ));
  } else {
    return null;
  }
};

export default ImageList;
