import React from "react";
import { StaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";

const ROADWAY_LIST = graphql`
  query RoadwayList {
    allFile(
      filter: {
        relativeDirectory: { eq: "roadway" }
        childImageSharp: { fluid: {} }
      }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`;

const Roadway = () => {
  return (
    <div>
      <h3 className="text-2xl font-inter_bold text-blue-500 mb-6">
        Roadway Express (1983-2007)
      </h3>
      <div className="font-inter_regular text-gray-700 text-lg text-justify leading-normal">
        <p className="mb-4">
          During the years I worked at Roadway Express, it was one of the
          largest trucking companies in the country, with over 30,000 employees
          and more than 600 terminals located coast to coast. (Today it is part
          of the trucking giant{" "}
          <a
            className="text-blue-500"
            href="http://yrc.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            YRC Freight
          </a>
          .)
        </p>
        <p>
          Some of my titles at Roadway included Business Systems Analyst and
          Technical Team Lead. I was responsible for determining business
          requirements and putting together technical direction for a group of
          about twelve developers, building a variety of applications for the
          company's thousand member national sales force.
        </p>
      </div>
      <div className="grid grid-cols-2 lg:grid-cols-3 gap-4 mt-6">
        <StaticQuery
          query={ROADWAY_LIST}
          render={({ allFile }) =>
            allFile.edges.map(({ node }) => (
              <Img
                className=""
                alt="Roadway Express"
                fluid={node.childImageSharp.fluid}
              />
            ))
          }
        />
      </div>
    </div>
  );
};

export default Roadway;
