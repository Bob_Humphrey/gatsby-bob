import React from "react";
import ImageList from "./ImageList";

const Projects = ({ props, bgColor }) => {
  return (
    <article className={bgColor + " w-full py-24"} key={props.directusId}>
      <div className="w-11/12 lg:w-1/2 mx-auto">
        <ImageList className="w-full" props={props} />
        <h3 className="w-full text-3xl font-inter_bold text-blue-500 text-center py-6">
          {props.name}
        </h3>
        <div className="lg:w-4/5 font-inter_regular text-gray-700 text-lg text-justify mx-auto">
          {props.description}
        </div>
      </div>
    </article>
  );
};

export default Projects;
