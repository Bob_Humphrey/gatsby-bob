import React from "react";
import { StaticQuery, graphql } from "gatsby";

const RECENT_LIST = graphql`
  query RecentList {
    allDirectusRecentWork(limit: 10) {
      edges {
        node {
          directusId
          name
          url
        }
      }
    }
  }
`;

const FooterRecentWork = () => {
  return (
    <ul>
      <StaticQuery
        query={RECENT_LIST}
        render={({ allDirectusRecentWork }) =>
          allDirectusRecentWork.edges.map(({ node, index }) => (
            <li className="" key={node.directusId}>
              <a
                className="text-white"
                href={node.url}
                target="_blank"
                rel="noopener noreferrer"
              >
                {node.name}
              </a>
            </li>
          ))
        }
      />
    </ul>
  );
};

export default FooterRecentWork;
