import React from "react";
import { Link } from "gatsby";
const ArticleLink = ({ post }) => (
  <div className="text-xl font-inter_regular text-gray-700 hover:text-blue-500 mb-6">
    <Link to={post.frontmatter.path}>
      <div className="grid md:grid-cols-5">
        <div className="md:col-span-4">{post.frontmatter.title}</div>
        <div className="md:col-span-1 md:text-right">
          {post.frontmatter.date}
        </div>
      </div>
    </Link>
  </div>
);
export default ArticleLink;
