import PropTypes from "prop-types";
import React from "react";
import SEO from "../components/seo";
import Header from "./header";
import Footer from "./Footer";

function Layout({ children }) {
  return (
    <div className="">
      <SEO
        keywords={[`web developer`, `programmer`]}
        title="Bob Humphrey Web Development"
      />
      <Header />

      <main className="w-full py-12">{children}</main>

      <Footer />
    </div>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired
};

export default Layout;
