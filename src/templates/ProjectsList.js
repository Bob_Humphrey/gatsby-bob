import React from "react";
import { graphql, Link } from "gatsby";
import Layout from "../components/Layout";
import Projects from "../components/Projects";
import PageNavigation from "../components/PageNavigation";

export const query = graphql`
  query ProjectsList($skip: Int!, $limit: Int!) {
    allDirectusRandall(skip: $skip, limit: $limit) {
      edges {
        node {
          directusId
          name
          description
          screenshots {
            directusId
            image {
              localFile {
                childImageSharp {
                  id
                  fluid {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

const ProjectsList = props => {
  const projects = props.data.allDirectusRandall.edges;
  const pageContext = props.pageContext;
  return (
    <Layout>
      <h2 className="w-11/12 lg:w-1/2 text-3xl font-inter_bold text-blue-800 leading-none mx-auto">
        Projects
      </h2>
      <div className="w-full font-inter_regular text-lg">
        <div className="w-11/12 lg:w-1/2 mx-auto py-8">
          <div className="no-underline pb-8">
            A partial list of some of the projects I completed while working at{" "}
            <Link to="/experience" className="text-blue-500">
              Randall Library
            </Link>
            .
          </div>
          <PageNavigation props={pageContext} />{" "}
        </div>
        {projects.map(({ node }, index) => {
          const bgColor = index % 2 ? "bg-white" : "bg-gray-50";
          return (
            <Projects key={node.directusId} props={node} bgColor={bgColor} />
          );
        })}
        <div className="w-11/12 lg:w-1/2 mx-auto pt-4">
          <PageNavigation props={pageContext} />{" "}
        </div>
      </div>
    </Layout>
  );
};

export default ProjectsList;
