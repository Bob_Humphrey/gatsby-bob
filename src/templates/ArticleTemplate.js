import React from "react";
import { graphql } from "gatsby";
import { motion } from "framer-motion";
import Layout from "../components/Layout";

export default function Template({
  data // this prop will be injected by the GraphQL query below.
}) {
  const variants = {
    hidden: {
      opacity: 0
    },
    visible: { opacity: 1 },
    transition: { duration: 1 }
  };
  const { markdownRemark } = data; // data.markdownRemark holds your post data
  const { frontmatter, html } = markdownRemark;
  return (
    <Layout>
      <motion.div initial="hidden" animate="visible" variants={variants}>
        <div className="w-11/12 lg:w-2/5 mx-auto mb-10">
          <div className="article">
            <h2 className="text-3xl font-inter_bold text-blue-800 leading-none mb-2">
              {frontmatter.title}
            </h2>
            <div className="text-base font-inter_light text-gray-700 mb-10">
              {frontmatter.date}
            </div>
            <div
              className="text-lg font-inter_regular text-gray-700 text-justify"
              dangerouslySetInnerHTML={{ __html: html }}
            />
          </div>
        </div>
      </motion.div>
    </Layout>
  );
}

export const pageQuery = graphql`
  query($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date(formatString: "MMMM D, YYYY")
        path
        title
      }
    }
  }
`;
