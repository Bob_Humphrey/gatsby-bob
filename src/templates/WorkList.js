import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/Layout";
import RecentWork from "../components/RecentWork";
import PageNavigation from "../components/PageNavigation";

export const query = graphql`
  query WorkList($skip: Int!, $limit: Int!) {
    allDirectusRecentWork(skip: $skip, limit: $limit) {
      edges {
        node {
          directusId
          name
          url
          code_url
          additional_code_url
          learn_more_url
          tags
          description
          screenshots {
            directusId
            image {
              localFile {
                childImageSharp {
                  id
                  fluid {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

const WorkList = props => {
  const projects = props.data.allDirectusRecentWork.edges;
  const pageContext = props.pageContext;
  return (
    <Layout>
      <h2 className="w-11/12 lg:w-1/2 text-3xl font-inter_bold text-blue-800 leading-none mx-auto">
        Recent Work
      </h2>
      <div className="w-full">
        <div className="w-11/12 lg:w-1/2 mx-auto py-8">
          <PageNavigation props={pageContext} />{" "}
        </div>
        {projects.map(({ node }, index) => {
          const bgColor = index % 2 ? "bg-white" : "bg-gray-50";
          return (
            <RecentWork key={node.directusId} props={node} bgColor={bgColor} />
          );
        })}
        <div className="w-11/12 lg:w-1/2 mx-auto pt-4">
          <PageNavigation props={pageContext} />{" "}
        </div>
      </div>
    </Layout>
  );
};

export default WorkList;
