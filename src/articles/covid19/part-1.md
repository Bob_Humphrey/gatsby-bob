---
path: "/articles/covid19/part-1"
date: "2020-06-01"
title: "Building a Covid-19 Dashboard with Laravel"
cover: null
published: true
---

[Laravel](https://laravel.com/) is great for building data driven applications. As an example of this, over the last couple of weeks, I put together my own Covid-19 dashboard, built entirely using the framework.

For my data source, I chose the [COVID Tracking Project](https://covidtracking.com/), an organization described by its website as "made up of hundreds of volunteer data-gatherers, developers, scientists, reporters, designers, editors, and other dedicated contributors." Every day, data is collected from the states, territories, and District of Columbia, and posted on their website. And, since the data is also made availabe through an API, my application can automatically update itself on a regular basis without any intervention on my part.

![Data from the COVID-Project website](data.png)

This snapshot from their website shows the kind of state level data that is collected every day, including number of deaths, number of tests, and confirmed cases.

## Measurements

I started my project by making a list of useful measurements that I could extrapolate from the basic data provided by the API. Here is the list, organized into three broad categories.

**How bad is the epidemic?**

- total number of deaths
- total deaths per capita
- seven day total deaths
- seven day average deaths
- daily deaths
- total number of cases
- total cases per capita
- seven day total cases
- seven day average cases
- daily new cases

**Is it getting better or worse?**

- seven day deaths percent increase
- fourteen day trend (comparing the current seven day average number of deaths to the average from fourteen days ago)
- days for deaths to double (based on the seven day deaths increase, how many days until the entire number of deaths is twice as large as it is now)
- seven day cases percent increase
- fourteen day trend (comparing the current seven day average number of new cases to the average from fourteen days ago)
- days for cases to double (based on the seven day cases increase, how many days until the entire number of cases is twice as large as it is now)

**Is there enough testing?**

- number of tests
- number of tests per capita
- seven day tests
- seven day tests average
- daily tests
- percentage of positive tests (tests that find infected persons)

It would also be helpful to be able to track the hospitalization information for each of the states, but the COVID-Project website states that "across the country, this reporting is sparse. In short: it is impossible to assemble anything resembling the real statistics for hospitalizations, ICU admissions, or ventilator usage across the United States."

## Overview

My project covers the United States and its territories, and I realize that the USA is not the entire world, and so this app reflects only a small portion of the global pandemic, and leaves out more than it includes. However, this is the country where I live, we currently have the most suffering and death in the world from this terrible disease, and so this is where I chose to focus my efforts.

The website is very simple and consists of four pages:

- A **home page** that provides key information

- A **measurements page** that focuses on a single measurement (from the list above) on a particular day

- A **history page** that details the daily change in a given measurement since the beginning of the epidemic

- A **state page** that provides a listing of all the measurements for a patricular state on a single day

Each of these pages has multiple links to each of the other pages.

## Main Menu

The main menu is on the left side of the page, and it has links to the **home page**, all of the **measurement pages**, and all of the **state pages**.

![Main menu](mainmenu.png)

## Date Menu

There is a date menu at the top of each of the **measurement pages** and **state pages**. These pages all display a single day at a time, and the date menu allows you to navigate through the days, going forward or backward one day or one week at a time. You can also jump to the most recent date, or back to the beginning of the measurement period, which is March 16th in this application.

![Date menu](datemenu.png)

## Data Presentation

The application displays information using a series of maps, graphs, and tables. All of the measurements are summarized into one of nine different categories. Each category is assigned a different color, with red indicating relatively worse results, and green indicating relatively better results. The maps and tables are then color coded using these categories.

## Home Page

![Home page](home.png)

The purpose of the home page is to provide a quick national overview for a single day. The main feature of the page consists of four maps representing what I believe to be the most important information available in the application.

Two of the maps show the seven day average of new cases and deaths, and these are meant to show the current state of the epidemic.

Another pair of maps show the fourteen day trend, created by comparing the current seven day averages to those from two weeks earlier. These reveal where the disease is getting better and where the disease is getting worse.

## Measurement page

![Measurement web page](cases.png)

The page above is an example of one of the views focused on a single measurement. This particular page shows the total number of confirmed cases for each states and territory as of May 26, 2020. Each state and territory is ranked from the most cases to the least.

On each line of the data table there is a calendar icon. Clicking this icon brings up another page with the historical information for that measurement and that state. For example, clicking on the calendar icon for the state of Louisiana brings up the page shown below.

## History Page

![Total cases history for Louisiana web page](louisiana.png)

The history page shows the changes in one particular measurement for a single state, in graph and table formats, from the beginning of the epidemic through the current day.

## State Page

![State snapshot for Ohio web page](ohio.png)

The state page shows all the measurements for an individual state or territory on a given day . There is also a set of graphs showing changes in the seven day averages and fourteen day trends.

## Back end

As I mentioned above, this entire project was built using Laravel.

Three times a day, Laravel automatically runs a job to query the COVID-Project API. The returned dataset is immediately loaded into a MySQL database.

Laravel then kicks off a series of jobs that transforms this basic data into the various measurements detailed above. The sequence of steps looks like this:

- The counts are compared to population size to arrive at per capita numbers.

- Seven day averages are computed. These help smooth out some of the sharp ups and downs caused by delays and lags in reporting.

- Seven day increase is computed.

- Seven day averages are compared to the seven day averages from 14 days earlier. This indicates whether the spread of the disease is increasing or decreasing.

- Doubling rates are estimated. Based on the previous seven day increase, how long until the already bad numbers are twice as bad.

Finally, there are a number of steps that are executed to separate all of the statistics into categories and rankings. These make it easier to present the various data using color coded maps and sorted tables.

## Front end components

The front end of the app was also built with Laravel, using the framework's easy to code and easy to maintain components. Here is how the components are structured for the **measurements page** shown above.

![Component structure](components.png)

The components can then be arranged into a layout using Laravel Blade component tags.

```html
<div class="grid grid-cols-3">
  <div class="col-span-1">
    <x-title />
    <x-menu />
    <x-credits />
    <x-articles />
    <x-author />
  </div>
  <div class="col-span-2">
    <x-pageTitle
      :pageTitle="$pageTitle"
      :date="$date"
      pageType="day"
      :path="$path"
    />
    <x-dateMenu :path="$path" />
    <x-map :stateData="$stateData" />
    <x-scaleDisplay :scale="$scale" />
    <x-dayDataTable
      :stateData="$stateData"
      :usData="$usData"
      :path="$path"
      :date="$date"
    />
  </div>
</div>
```

Some of the components used to build the project include:

- **Maps** - Using an open source SVG file availble on Wikipedia.

- **Charts** - Using [Larapex Charts](https://arielmejiadev.github.io/LarapexCharts-Documentation/).

- **Tables** - Using HTML.

[TailwindCSS](https://tailwindcss.com/) was used for styling, and [AlpineJs](https://github.com/alpinejs/alpine) was used to add a little bit of JavaScript behavior, for things like hiding and displaying the menu on small screens.

## Useful Techniques

This project demonstrates a number of useful techniques for using Laravel to create a data driven application. Some of these include:

- Creating jobs that automatically perform tasks, such as querying an api and updating a database, on a daily schedule
- Reading a CSV file and loading the information into a database
- Creating categories and assigning measurements to them
- Building components
- Making charts and graphs
- Creating a color coded map
- Using icons
- Adding a little bit of interactivity with Alpine js

## Links to the app and code

You can check out a working version of the app [here](https://covid19.bob-humphrey.com/).

The code for the app is on [Gitlab](https://gitlab.com/Bob_Humphrey/laravel-covid19).
