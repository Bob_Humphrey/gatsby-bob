---
path: "/articles/inter-fonts/article"
date: "2020-05-03"
title: "Five Reasons to Give Inter Fonts a Try"
cover: null
published: true
---

As a typical coder, I've always found it challenging to design my sites in a professional manner. However, I feel like I've seen some recent improvement in this department, and I give much of the credit for this to my recent discovery of the [Inter](https://rsms.me/inter/) family of web fonts. Created by [Rasmus Andersson](https://rsms.me/about/), a designer at [Figma](https://www.figma.com/blog/the-birth-of-inter/), these fonts were made with the express intention of making text easier to read on computer screens.

As an example, consider this simple design for a short biography of singer-songwriter Mary Chapin Carpenter, created using the default san serif font on my PC.

![Mary Chapin Carpenter biography created with default san serif font on my PC](mcc-system.png)

Now here's that same design, but recreated using Inter fonts.

![Mary Chapin Carpenter biography created with Inter fonts](mcc-inter.png)

Both designs are fine, and the differences are subtle, but the second design comes a little closer to what I want, thanks entirely to the greater range of choice I have when using Inter.

So let's consider some of the reasons why you might want to give these wonderful fonts a try.

## Wide Range of Weights and Styles

The Inter family consists of 9 different weights, each with its associated italic, for a total of 18 different styles! This _really_ improves our ability to express ourselves.

Most websites are created to communicate information, and the main tool for accomplishing this is through words and text. To facilitate this communication, we use HTML to organize our sites into logical elements, such as articles, headings, and paragraphs.

Artful use of typography supports this organization and makes it immediately clear and understandable to our readers. We want a large font with a heavy weight to give emphasis to the h1 heading. We also want the h2 heading to stand out, but with a smaller size and less heavy weight so that it's clearly subordinate to the main heading. Having a wide range of font weights at our disposal improves our ability to convey our intent and communicate clearly.

At the other end of the weight range scale, the lightweight fonts can also be very useful. Small text used for things like footnotes and labels can look a whole lot better when rendered in one of the Thin or Extra Light fonts

## Completeness

Inter fonts include 2528 different characters, with support for the Latin, Greek, and Cyrillic alphabets!

Over 150 different languages are supported!

## Readability

Using a single font family can lead to more harmonious designs. Mixing different fonts can be tricky for us non-designers, but with Inter fonts we have fewer reasons to do this. Using a single font family increases the likelihood that all the elements on the page are going to look good together, and this makes our sites more attractive, readable and easier to understand.

## Open Source and Free

Let's face it, commercial fonts can be quite expensive. They are certainly worth the cost to the shops and companies that are making a profit from their work. But for us developers, and especially those who are just getting started as coders, it's probably impossible to justify that kind of investment.

One of the great things about Inter fonts is that they are open source and completely free to use for just about any purpose. The [SIL Open Font License 1.1](https://choosealicense.com/licenses/ofl-1.1/) contains all the details. With Inter fonts, you get many of the benefits of a commercial font family without the usual associated cost.

## Easy to Use

It's easy to get started with Inter fonts. You can download them and add them to your project (instructions below), or you can just grab them from [Google](https://fonts.google.com/specimen/Inter) if that works better for you.

## Using with Tailwind CSS

Here is how you can add these fonts to any project that uses [Tailwind CSS](https://tailwindcss.com/).

- Download the fonts from the [Inter website](https://rsms.me/inter/). There is a link for downloading on the top menu bar.

- Extract the compressed folder. Copy the files in the Inter Web directory to the same folder that contains your tailwind CSS file.

- Add the fonts to the **style.css** file, which will look like this. Of course, you don't need to add every font, just the ones you're going to use.

```css
@tailwind base;

@font-face {
  font-family: "inter_black";
  src: url("Inter-Black.woff") format("woff"), url("Inter-Black.woff2") format("woff2");
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}

@font-face {
  font-family: "inter_black_italic";
  src: url("Inter-BlackItalic.woff") format("woff"), url("Inter-BlackItalic.woff2")
      format("woff2");
  font-weight: normal;
  font-style: italic;
  font-display: swap;
}

@font-face {
  font-family: "inter_bold";
  src: url("Inter-Bold.woff") format("woff"), url("Inter-Bold.woff2") format("woff2");
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}

@font-face {
  font-family: "inter_bold_italic";
  src: url("Inter-BoldItalic.woff") format("woff"), url("Inter-BoldItalic.woff2")
      format("woff2");
  font-weight: normal;
  font-style: italic;
  font-display: swap;
}

@font-face {
  font-family: "inter_extrabold";
  src: url("Inter-ExtraBold.woff") format("woff"), url("Inter-ExtraBold.woff2")
      format("woff2");
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}

@font-face {
  font-family: "inter_extrabold_italic";
  src: url("Inter-ExtraBoldItalic.woff") format("woff"), url("Inter-ExtraBoldItalic.woff2")
      format("woff2");
  font-weight: normal;
  font-style: italic;
  font-display: swap;
}

@font-face {
  font-family: "inter_extralight";
  src: url("Inter-ExtraLight.woff") format("woff"), url("Inter-ExtraLight.woff2")
      format("woff2");
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}

@font-face {
  font-family: "inter_extralight_italic";
  src: url("Inter-ExtraLightItalic.woff") format("woff"), url("Inter-ExtraLightItalic.woff2")
      format("woff2");
  font-weight: normal;
  font-style: italic;
  font-display: swap;
}

@font-face {
  font-family: "inter_italic";
  src: url("Inter-Italic.woff") format("woff"), url("Inter-Italic.woff2") format("woff2");
  font-weight: normal;
  font-style: italic;
  font-display: swap;
}

@font-face {
  font-family: "inter_light";
  src: url("Inter-Light.woff") format("woff"), url("Inter-Light.woff2") format("woff2");
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}

@font-face {
  font-family: "inter_light_italic";
  src: url("Inter-LightItalic.woff") format("woff"), url("Inter-LightItalic.woff2")
      format("woff2");
  font-weight: normal;
  font-style: italic;
  font-display: swap;
}

@font-face {
  font-family: "inter_medium";
  src: url("Inter-Medium.woff") format("woff"), url("Inter-Medium.woff2") format("woff2");
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}

@font-face {
  font-family: "inter_medium_italic";
  src: url("Inter-MediumItalic.woff") format("woff"), url("Inter-MediumItalic.woff2")
      format("woff2");
  font-weight: normal;
  font-style: italic;
  font-display: swap;
}

@font-face {
  font-family: "inter_regular";
  src: url("Inter-Regular.woff") format("woff"), url("Inter-Regular.woff2")
      format("woff2");
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}

@font-face {
  font-family: "inter_semibold";
  src: url("Inter-SemiBold.woff") format("woff"), url("Inter-SemiBold.woff2")
      format("woff2");
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}

@font-face {
  font-family: "inter_semibold_italic";
  src: url("Inter-SemiBoldItalic.woff") format("woff"), url("Inter-SemiBoldItalic.woff2")
      format("woff2");
  font-weight: normal;
  font-style: italic;
  font-display: swap;
}

@font-face {
  font-family: "inter_thin";
  src: url("Inter-Thin.woff") format("woff"), url("Inter-Thin.woff2") format("woff2");
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}

@font-face {
  font-family: "inter_thin_italic";
  src: url("Inter-ThinItalic.woff") format("woff"), url("Inter-ThinItalic.woff2")
      format("woff2");
  font-weight: normal;
  font-style: italic;
  font-display: swap;
}

@tailwind components;

@tailwind utilities;
```

- Update the Tailwind configuration file **tailwind.config.js**. Again, just add the fonts you're going to need.

```js
module.exports = {
  theme: {
    extend: {},
    fontFamily: {
      inter_black: [`inter_black`],
      inter_black_italic: [`inter_black_italic`],
      inter_bold: [`inter_bold`],
      inter_bold_italic: [`inter_bold_italic`],
      inter_extrabold: [`inter_extrabold`],
      inter_extrabold_italic: [`inter_extrabold_italic`],
      inter_extralight: [`inter_extralight`],
      inter_extralight_italic: [`inter_extralight_italic`],
      inter_italic: [`inter_italic`],
      inter_light: [`inter_light`],
      inter_light_italic: [`inter_light_italic`],
      inter_medium: [`inter_medium`],
      inter_medium_italic: [`inter_medium_italic`],
      inter_regular: [`inter_regular`],
      inter_semibold: [`inter_semibold`],
      inter_semibold_italic: [`inter_semibold_italic`],
      inter_thin: [`inter_thin`],
      inter_thin_italic: [`inter_thin_italic`],
    },
  },
  variants: {},
  plugins: [],
}
```

- Compile the **style.css** and **tailwind.config.js** files into your **tailwind.css** file.

```bash
npx tailwind build style.css -o src/css/tailwind.css
```

- To use the fonts, just add the one you want to the class attribute of your HTML elements.

```html
<h2 class="font-inter_semibold text-2xl"></h2>
```

- or for React:

```html
<h2 className="font-inter_semibold text-2xl"></h2>
```

## Examples of the available weights

Below are the nine weights available, with their regular and italic styles.

![Inter thin font](inter-thin.png)
Thin

![Inter extra light font](inter-extralight.png)
Extra Light

![Inter light font](inter-light.png)
Light

![Inter regular font](inter-regular.png)
Regular

![Inter medium font](inter-medium.png)
Medium

![Inter semi bold font](inter-semibold.png)
Semi Bold

![Inter bold font](inter-bold.png)
Bold

![Inter extra bold font](inter-extrabold.png)
Extra Bold

![Inter black font](inter-black.png)
Black

## Websites using Inter fonts

Need a little more convincing? Check out this sampling of websites that make good use of the Inter font family. Maybe they will inspire you with some of the possibilities for using these beautiful fonts.

- [Github](https://github.com/)
- [Mozilla](https://mozilla.design/firefox/typography/)
- [Workcapsule](https://workcapsule.io/)
- [Linear](https://linear.app/)
- [Root UI Kit](https://rootwireframekit.com/)

## Credits

**Learn more about Inter fonts at https://rsms.me/inter**

**Heading photo by [Daria Shevtsova](https://www.pexels.com/@daria) from [Pexels](https://www.pexels.com/photo/assorted-wooden-alphabets-inside-the-crate-709830/)**

**Mary Chapin Carpenter information from [Wikipedia](https://en.wikipedia.org/wiki/Mary_Chapin_Carpenter)**

**Quote used to illustrate example weights by [Mary Chapin Carpenter](https://www.marychapincarpenter.com/) from ["The Hard Way", _Come On, Come On_](https://mary-chapin-carpenter.myshopify.com/collections/music/products/come-on-come-on-cd)**
