// Create pages for docs and blogs separately using two separate
// queries. We use the `graphql` function which returns a Promise
// and ultimately resolve all of them using Promise.all(Promise[])onst path = require(`path`);

const path = require(`path`);

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;
  const destinationTemplate = path.resolve(
    `./src/page_nodes/DestinationDetail.js`
  );
  const attractionGroupTemplate = path.resolve(
    `./src/page_nodes/AttractionGroupDetail.js`
  );

  const destinations = graphql(`
    {
      allDirectusDestination(sort: { fields: directusId }) {
        edges {
          node {
            directusId
          }
        }
      }
    }
  `).then(result => {
    if (result.errors) {
      Promise.reject(result.errors);
    }

    result.data.allDirectusDestination.edges.forEach(({ node }) => {
      createPage({
        path: `/destinations/${node.directusId}`,
        component: destinationTemplate,
        context: {
          directusId: node.directusId
        }
      });
    });
  });

  const attractionGroups = graphql(`
    {
      allDirectusDestination {
        edges {
          node {
            attraction_groups {
              directusId
            }
          }
        }
      }
    }
  `).then(result => {
    if (result.errors) {
      Promise.reject(result.errors);
    }

    result.data.allDirectusDestination.edges.forEach(({ node }) => {
      let groups = node.attraction_groups;
      groups.forEach(g => {
        let id = g.directusId;
        createPage({
          path: `/attraction_groups/${id}`,
          component: attractionGroupTemplate,
          context: {
            directusId: id
          }
        });
      });
    });
  });

  return Promise.all([destinations, attractionGroups]);
};
